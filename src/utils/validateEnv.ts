import {
  cleanEnv, port, str, url,
} from 'envalid';

const validateEnv = () => {
  cleanEnv(process.env, {
    NODE_ENV: str({ choices: ['development', 'test', 'production', 'staging'] }),
    PORT: port(),
    SECRET_KEY: str(),
    DATABASE_URL: url(),
  });
};

export default validateEnv;
