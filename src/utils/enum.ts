import { VideoResolutionMap } from '@/interfaces/video.interface';

export const VideoSize: VideoResolutionMap = {
  144: '256x144',
  240: '320x240',
  360: '640x480',
  480: '720x480',
  720: '1280x720',
  1080: '1920x1080',
};
