import { AuthController } from '@controllers/auth.controller';
import { UserController } from '@controllers/user.controller';
import { VideoController } from '@controllers/video.controller';
import { CommentController } from '@controllers/comment.controller';
import validateEnv from '@utils/validateEnv';
import App from '@/app';

validateEnv();

const app = new App([
  AuthController,
  UserController,
  VideoController,
  CommentController,
]);
app.listen();
