import { Response } from 'express';
import {
  Controller, Body, Post, UseBefore, Res, HttpCode,
} from 'routing-controllers';
import { LoginUserDto } from '@dtos/users.dto';
import { validationMiddleware } from '@middlewares/validation.middleware';
import AuthService from '@services/auth.service';
import { OpenAPI } from 'routing-controllers-openapi';

@Controller()
export class AuthController {
  public authService = new AuthService();

  @Post('/auth')
  @OpenAPI({ summary: 'Logs user in, sets authorization cookie and sends back the token in body' })
  @UseBefore(validationMiddleware(LoginUserDto, 'body'))
  @HttpCode(201)
  async logIn(@Res() res: Response, @Body() userData: LoginUserDto) {
    const { cookie, token } = await this.authService.login(userData);

    res.setHeader('Set-Cookie', [cookie]);
    return { message: 'OK', data: token };
  }
}
