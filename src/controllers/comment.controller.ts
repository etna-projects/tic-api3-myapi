import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  QueryParams,
  Req,
  UseBefore,
} from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';
import { CreateCommentDto } from '@/dtos/comment.dto';
import { PagerDto } from '@/dtos/pager.dto';
import { validationMiddleware } from '@/middlewares/validation.middleware';
import CommentService from '@/services/comment.service';
import authMiddleware from '@/middlewares/auth.middleware';
import { RequestWithUser } from '@/interfaces/auth.interface';

@Controller()
export class CommentController {
  public commentService = new CommentService();

  @Post('/video/:id/comment')
  @OpenAPI({ summary: 'Create a new comment' })
  @HttpCode(201)
  @UseBefore(authMiddleware, validationMiddleware(CreateCommentDto, 'body'))
  async createComment(@Req() { user }: RequestWithUser, @Body() commentData: CreateCommentDto, @Param('id') videoId: number) {
    const data = await this.commentService.createComment(commentData, user, videoId);
    return { message: 'Ok', data };
  }

  @Get('/video/:id/comments')
  @OpenAPI({ summary: 'Get all comments of a video' })
  @HttpCode(200)
  @UseBefore(authMiddleware, validationMiddleware(PagerDto, 'query'))
  async getComments(@Param('id') videoId: number, @QueryParams({ required: false }) query: PagerDto) {
    const { comments, pager } = await this.commentService.listComments(videoId, query);
    return { message: 'OK', data: comments, pager };
  }
}
