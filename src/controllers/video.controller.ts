import {
  Controller,
  HttpCode,
  Param,
  Post,
  UploadedFile,
  Req,
  Get,
  QueryParams,
  Put,
  UseBefore,
  Body,
  Delete,
  Patch,
} from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';
import { Request } from 'express';
import { upload } from '@/config';
import {
  CreateVideoDto, ListVideosQuery, UpdateVideoDto,
} from '@/dtos/video.dto';
import { validationMiddleware } from '@/middlewares/validation.middleware';
import VideoService from '@/services/video.service';
import { PagerDto } from '@/dtos/pager.dto';
import { RequestWithUser } from '@/interfaces/auth.interface';
import authMiddleware from '@/middlewares/auth.middleware';

@Controller()
export class VideoController {
  public videoService = new VideoService();

  @Patch('/video/:id')
  @OpenAPI({ summary: 'Scale video' })
  @HttpCode(200)
  async scaleVideo(
  @Param('id') videoId: number,
    @Req() { body }: Request,
    @UploadedFile('file', { options: upload.options }) file: Express.Multer.File,
  ) {
    const data = await this.videoService.scaleVideo(videoId, body, file);
    return { message: 'OK', data };
  }

  @Post('/user/:id/video')
  @OpenAPI({ summary: 'Create a new video' })
  @HttpCode(201)
  @UseBefore(authMiddleware)
  async createVideo(
  @Param('id') userId: number,
    @Req() { body: videoData, user }: RequestWithUser<CreateVideoDto>,
    @UploadedFile('source', { options: upload.options }) file: Express.Multer.File,
  ) {
    const data = await this.videoService.createVideo(file, videoData, userId, user);
    return { message: 'Ok', data };
  }

  @Get('/videos')
  @OpenAPI({ summary: 'Get all videos' })
  @HttpCode(200)
  async getVideos(@QueryParams({ required: false }) query: ListVideosQuery) {
    const { videos, pager } = await this.videoService.listVideos(query);
    return { message: 'OK', data: videos, pager };
  }

  @Get('/user/:id/videos')
  @OpenAPI({ summary: 'Get all videos of a user' })
  @HttpCode(200)
  @UseBefore(validationMiddleware(PagerDto, 'query'))
  async getVideosByUser(@Param('id') userId: number, @QueryParams({ required: false }) query: PagerDto) {
    const { videos, pager } = await this.videoService.listVideosByUser(query, userId);
    return { message: 'OK', data: videos, pager };
  }

  @Put('/video/:id')
  @OpenAPI({ summary: 'Update a video' })
  @HttpCode(200)
  @UseBefore(authMiddleware)
  async updateVideo(@Req() { user }: RequestWithUser, @Param('id') id: number, @Body() videoData: UpdateVideoDto) {
    const data = await this.videoService.updateVideo(id, user, videoData);
    return { message: 'OK', data };
  }

  @Delete('/video/:id')
  @OpenAPI({ summary: 'Delete a video' })
  @HttpCode(204)
  @UseBefore(authMiddleware)
  async deleteVideo(@Req() { user }: RequestWithUser, @Param('id') id: number) {
    await this.videoService.deleteVideo(id, user);
    return null;
  }
}
