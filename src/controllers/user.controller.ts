import {
  Controller,
  Param,
  Body,
  Get,
  Post,
  Put,
  Delete,
  HttpCode,
  UseBefore,
  QueryParams,
  Req,
} from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';
import { CreateUserDto, ListUsersQuery, UpdateUserDto } from '@dtos/users.dto';
import { User } from '@prisma/client';
import UserService from '@services/users.service';
import { validationMiddleware } from '@middlewares/validation.middleware';
import authMiddleware from '@/middlewares/auth.middleware';
import { RequestWithUser } from '@/interfaces/auth.interface';
import { UserResponse } from '@/interfaces/reponses.interface';

@Controller()
export class UserController {
  public userService = new UserService();

  @Get('/users')
  @OpenAPI({ summary: 'Return a list of users' })
  @HttpCode(200)
  @UseBefore(validationMiddleware(ListUsersQuery, 'query'))
  async getUsers(@QueryParams({ required: false }) query: ListUsersQuery) {
    const { users, pager } = await this.userService.findUserByPseudo(
      query.page,
      query.perPage,
      query.pseudo,
    );
    return { message: 'OK', data: users, pager };
  }

  @Get('/user/:id')
  @OpenAPI({ summary: 'Return find a user' })
  @UseBefore(authMiddleware)
  async getUserById(@Param('id') userId: number) {
    const findOneUserData: Partial<User> = await this.userService.findUserById(userId);
    return { message: 'OK', data: findOneUserData };
  }

  @Post('/user')
  @HttpCode(201)
  @UseBefore(validationMiddleware(CreateUserDto, 'body'))
  @OpenAPI({ summary: 'Create a new user' })
  async createUser(@Body() userData: CreateUserDto) {
    const user = await this.userService.createUser(userData);
    return { message: 'Ok', data: user };
  }

  @Put('/user/:id')
  @UseBefore(authMiddleware, validationMiddleware(UpdateUserDto, 'body'))
  @OpenAPI({ summary: 'Update a user' })
  @HttpCode(200)
  async updateUser(@Req() req: RequestWithUser, @Param('id') userId: number, @Body() userData: UpdateUserDto) {
    const updateUserData: UserResponse = await this.userService.updateUser(
      userId,
      userData,
      req.user,
    );
    return { message: 'OK', data: updateUserData };
  }

  @Delete('/user/:id')
  @OpenAPI({ summary: 'Delete a user' })
  @HttpCode(204)
  @UseBefore(authMiddleware)
  async deleteUser(@Req() req: RequestWithUser, @Param('id') userId: number) {
    await this.userService.deleteUser(userId, req.user);
    return null;
  }
}
