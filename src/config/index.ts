import { config } from 'dotenv';
import { diskStorage } from 'multer';
import path from 'path';

config({ path: `.env.${process.env.NODE_ENV || 'development'}.local` });

export const upload = {
  options: {
    storage: diskStorage({
      destination: path.join(__dirname, '../../public/videos'),
      filename: (req, file, callback) => {
        callback(null, `${Date.now()}_${file.originalname.replace(/[^a-zA-Z0-9. ]/g, '').split(' ').join('_')}`);
      },
    }),
    limits: {},
  },
};

export const CREDENTIALS = process.env.CREDENTIALS === 'true';
export const {
  NODE_ENV, PORT, SECRET_KEY, LOG_FORMAT, LOG_DIR, ORIGIN,
} = process.env;
