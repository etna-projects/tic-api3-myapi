import {
  PrismaClient, User, Comment,
} from '@prisma/client';
import { CreateCommentDto } from '@/dtos/comment.dto';
import { PagerDto } from '@/dtos/pager.dto';
import { HttpException } from '@/exceptions/HttpException';
import { PagerInfo } from '@/interfaces/utils.interface';
import { CommentResponse } from '@/interfaces/reponses.interface';

const select = {
  id: true,
  body: true,
  user: {
    select: {
      id: true,
      pseudo: true,
      email: true,
      created_at: true,
      username: true,
    },
  },
};

export default class CommentService {
  public comments = new PrismaClient().comment;

  public videos = new PrismaClient().video;

  public async createComment(
    commentData: CreateCommentDto,
    user: User,
    videoId: number,
  ): Promise<CommentResponse> {
    const foundVideo = await this.videos.findFirst({ where: { id: videoId } });
    if (!foundVideo) throw new HttpException(404, 'Not found');
    const comment = await this.comments.create({
      data: {
        body: commentData.body,
        video: {
          connect: {
            id: foundVideo.id,
          },
        },
        user: {
          connect: {
            id: user.id,
          },
        },
      },
      select,
    });
    return comment;
  }

  public async listComments(
    videoId: number,
    { page = 1, perPage = 5 }: PagerDto,
  ): Promise<{ comments: CommentResponse[]; pager: PagerInfo }> {
    if (page <= 0) {
      throw new HttpException(
        400,
        'Bad Request',
        2000,
        ['page must be greater than 0'],
      );
    }
    const count = await this.comments.count({
      where: {
        video: {
          id: videoId,
        },
      },
      orderBy: {
        id: 'desc',
      },
    });
    if (page > 1 && page > Math.ceil(count / perPage)) {
      throw new HttpException(
        400,
        'Bad Request',
        2001,
        [`page must be lesser than ${Math.ceil(count / perPage)}`],
      );
    }
    const foundCommentsData: CommentResponse[] = await this.comments.findMany({
      take: perPage,
      skip: perPage * ((page || 1) - 1),
      where: {
        video: {
          id: videoId,
        },
      },
      orderBy: {
        id: 'desc',
      },
      select,
    });
    return {
      comments: foundCommentsData,
      pager: { current: page || 1, total: Math.ceil(count / perPage) || 1 },
    };
  }
}
