import { hash } from 'bcrypt';
import { PrismaClient, User } from '@prisma/client';
import { CreateUserDto, UpdateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty } from '@utils/util';
import { PagerInfo } from '@/interfaces/utils.interface';
import { UserResponse } from '@/interfaces/reponses.interface';

class UserService {
  public users = new PrismaClient().user;

  public async findAllUser(page?: number, perPage?: number): Promise<UserResponse[]> {
    const findAllUsersData: UserResponse[] = await this.users.findMany({
      take: perPage,
      skip: perPage * (page - 1),
      select: {
        id: true,
        pseudo: true,
        email: true,
        created_at: true,
        username: true,
      },
    });
    return findAllUsersData;
  }

  public async findUserByPseudo(
    page?: number,
    perPage = 5,
    pseudo?: string,
  ): Promise<{ users: UserResponse[]; pager: PagerInfo }> {
    if (page <= 0) {
      throw new HttpException(
        400,
        'Bad Request',
        2000,
        ['page must be greater than 0'],
      );
    }
    const count = await this.users.count({
      where: {
        pseudo: {
          contains: pseudo,
        },
      },
      orderBy: {
        id: 'desc',
      },
    });
    if (page > 1 && page > Math.ceil(count / perPage)) {
      throw new HttpException(
        400,
        'Bad Request',
        2001,
        [`page must not be lesser than ${Math.ceil(count / perPage)}`],
      );
    }
    const foundUsersData: UserResponse[] = await this.users.findMany({
      take: perPage,
      skip: perPage * ((page || 1) - 1),
      where: {
        pseudo: {
          contains: pseudo,
        },
      },
      orderBy: {
        id: 'desc',
      },
      select: {
        id: true,
        pseudo: true,
        email: true,
        created_at: true,
        username: true,
      },
    });
    return {
      users: foundUsersData,
      pager: { current: page || 1, total: Math.ceil(count / perPage) || 1 },
    };
  }

  public async findUserById(userId: number): Promise<UserResponse> {
    const findUser: UserResponse = await this.users.findUnique({
      where: { id: userId },
      select: {
        id: true,
        pseudo: true,
        email: true,
        created_at: true,
        username: true,
      },
    });
    if (!findUser) throw new HttpException(404, 'Not found');

    return findUser;
  }

  public async createUser(userData: CreateUserDto): Promise<UserResponse> {
    const findUser: User[] = await this.users.findMany({
      where: {
        OR: [
          { email: { equals: userData.email } },
          { username: { equals: userData.username } },
        ],
      },
    });
    if (findUser.length > 0) {
      if (findUser.find((user) => user.email === userData.email)) {
        throw new HttpException(400, 'Bad Request', 2002, ['email already exists']);
      }
      if (findUser.find((user) => user.username === userData.username)) {
        throw new HttpException(400, 'Bad Request', 2003, ['username already exists']);
      }
    }

    const hashedPassword = await hash(userData.password, 10);
    const createdUserData = await this.users.create({
      data: { ...userData, password: hashedPassword },
      select: {
        id: true,
        pseudo: true,
        email: true,
        created_at: true,
        username: true,
      },
    });
    return createdUserData;
  }

  public async updateUser(
    userId: number,
    userData: UpdateUserDto,
    user: Partial<User>,
  ): Promise<UserResponse> {
    if (user.id !== userId) throw new HttpException(401, 'Unauthorized');

    const findUser: User = await this.users.findUnique({ where: { id: userId } });
    if (!findUser) throw new HttpException(404, 'Not found');

    const isPasswordUpdated = !isEmpty(userData.password);
    if (isPasswordUpdated) {
      const hashedPassword = await hash(userData.password, 10);
      await this.users.update({
        where: { id: userId },
        data: {
          password: hashedPassword,
        },
      });
    }
    const updateUserData = await this.users.update({
      where: { id: userId },
      data: {
        email: isEmpty(userData.email) ? findUser.email : userData.email,
        pseudo: isEmpty(userData.pseudo) ? findUser.pseudo : userData.pseudo,
        username: isEmpty(userData.username) ? findUser.username : userData.username,
      },
      select: {
        id: true,
        pseudo: true,
        email: true,
        created_at: true,
        username: true,
      },
    });
    return updateUserData;
  }

  public async deleteUser(userId: number, user: User): Promise<UserResponse> {
    if (user.id !== userId) throw new HttpException(401, 'Unauthorized');

    const findUser: User = await this.users.findUnique({ where: { id: userId } });
    if (!findUser) throw new HttpException(404, 'Not found');

    const deleteUserData = await this.users.delete({
      where: { id: userId },
      select: {
        id: true,
        pseudo: true,
        email: true,
        created_at: true,
        username: true,
      },
    });
    return deleteUserData;
  }
}

export default UserService;
