import path from 'path';
import { PrismaClient, User } from '@prisma/client';
import {
  CreateVideoDto, FormatVideoDto, ListVideosQuery, ScaleVideoDto, UpdateVideoDto,
} from '@/dtos/video.dto';
import { HttpException } from '@/exceptions/HttpException';
import { isEmpty } from '@/utils/util';
import EditorService from './editor.service';
import { PagerInfo } from '@/interfaces/utils.interface';
import { PagerDto } from '@/dtos/pager.dto';
import { VideoResponse } from '@/interfaces/reponses.interface';
import { GenericVideoOutput } from '@/interfaces/video.interface';

const select = {
  id: true,
  name: true,
  source: true,
  duration: true,
  views: true,
  created_at: true,
  enabled: true,
  user: {
    select: {
      id: true,
      email: true,
      created_at: true,
      pseudo: true,
      username: true,
    },
  },
  video_format: true,
};
class VideoService {
  private videos = new PrismaClient().video;

  private videoFormats = new PrismaClient().video_format;

  private editor = new EditorService();

  // eslint-disable-next-line class-methods-use-this
  private formatVideoResponse(video: GenericVideoOutput): VideoResponse {
    return {
      id: video.id,
      source: video.source,
      views: video.views,
      created_at: video.created_at,
      enabled: video.enabled,
      user: {
        id: video.user.id,
        email: video.user.email,
        created_at: video.user.created_at,
        pseudo: video.user.pseudo,
        username: video.user.username,
      },
      format: {
        1080: video.video_format?.find((format) => format.code === '1080')?.uri || '',
        720: video.video_format?.find((format) => format.code === '720')?.uri || '',
        480: video.video_format?.find((format) => format.code === '480')?.uri || '',
        360: video.video_format?.find((format) => format.code === '360')?.uri || '',
        240: video.video_format?.find((format) => format.code === '240')?.uri || '',
        144: video.video_format?.find((format) => format.code === '144')?.uri || '',
      },
    };
  }

  public async scaleVideo(
    id: number,
    videoData: FormatVideoDto,
    file: Express.Multer.File,
  ): Promise<VideoResponse> {
    if (!videoData.format) throw new HttpException(400, 'Bad Request', 2002, ['format is required']);
    if (!['1080', '720', '480', '360', '240', '144'].includes(videoData.format)) throw new HttpException(400, 'Bad Request', 2002, ['format must be one of the following: 1080, 720, 480, 360, 240, 144']);
    const foundVideo = await this.videos.findFirst({
      where: { id },
    });
    if (!foundVideo) throw new HttpException(404, 'Not found');
    await this.videoFormats.create({
      data: {
        code: videoData.format,
        uri: path.join('/public/videos', file.filename),
        video: {
          connect: {
            id,
          },
        },
      },
    });
    const updatedVideo = await this.videos.findFirst({
      where: { id },
      select,
    });
    return this.formatVideoResponse(updatedVideo);
  }

  public async deleteVideo(id: number, user: User) {
    const foundVideo = await this.videos.findFirst({
      where: { id },
      include: { user: true },
    });
    if (!foundVideo) throw new HttpException(404, 'Not found');
    if (user.id !== foundVideo.user.id) throw new HttpException(401, 'Unauthorized');
    await this.videos.delete({
      where: { id },
    });
    await this.editor.deleteVideo(foundVideo.source);
  }

  public async updateVideo(
    id: number,
    user: User,
    videoData: UpdateVideoDto,
  ): Promise<VideoResponse> {
    const foundVideo = await this.videos.findFirst({
      where: { id },
      include: { user: true },
    });
    if (!foundVideo) throw new HttpException(404, 'Not found');
    if (user.id !== foundVideo.user.id) throw new HttpException(401, 'Unauthorized');
    const updated = await this.videos.update({
      where: { id },
      data: {
        name: isEmpty(videoData.name) ? undefined : videoData.name,
        user: isEmpty(videoData.user) ? undefined : {
          connect: {
            id: videoData.user,
          },
        },
      },
      select,
    });
    return this.formatVideoResponse(updated);
  }

  public async listVideosByUser(
    query: PagerDto,
    userId: number,
  ): Promise<{ videos: VideoResponse[]; pager: PagerInfo }> {
    const { page = 1, perPage = 5 } = query;
    if (page <= 0) {
      throw new HttpException(
        400,
        'Bad Request',
        2000,
        ['page must be greater than 0'],
      );
    }
    const count = await this.videos.count({
      where: {
        user: {
          id: userId,
        },
      },
      orderBy: {
        id: 'desc',
      },
    });
    if (page > 1 && page > Math.ceil(count / perPage)) {
      throw new HttpException(
        400,
        'Bad Request',
        2001,
        [`page must be lesser than ${Math.ceil(count / perPage)}`],
      );
    }
    const foundVideosData = await this.videos.findMany({
      take: perPage,
      skip: perPage * ((page || 1) - 1),
      where: {
        user: {
          id: userId,
        },
      },
      orderBy: {
        id: 'desc',
      },
      select,
    });
    return {
      videos: foundVideosData.map((video) => this.formatVideoResponse(video)),
      pager: { current: page || 1, total: Math.ceil(count / perPage) || 1 },
    };
  }

  public async listVideos(
    query: ListVideosQuery,
  ): Promise<{ videos: VideoResponse[]; pager: PagerInfo }> {
    const {
      page = 1, perPage = 5, duration, name, user,
    } = query;
    if (page <= 0) {
      throw new HttpException(
        400,
        'Bad Request',
        2000,
        ['page must be greater than 0'],
      );
    }
    const conditions = {
      duration: {
        equals: isEmpty(duration) ? undefined : duration,
      },
      name: isEmpty(name) ? undefined : {
        equals: name,
      },
      // eslint-disable-next-line no-nested-ternary
      user: isEmpty(user) ? undefined : !Number.isNaN(parseInt(user, 10))
        ? {
          id: parseInt(user, 10),
        }
        : typeof user === 'string' ? { username: { equals: user } } : undefined,
    };
    const count = await this.videos.count({
      where: conditions,
      orderBy: {
        id: 'desc',
      },
    });
    if (page > 1 && page > Math.ceil(count / perPage)) {
      throw new HttpException(
        400,
        'Bad Request',
        2001,
        [`page must be lesser than ${Math.ceil(count / perPage)}`],
      );
    }
    const foundVideosData = await this.videos.findMany({
      take: perPage,
      skip: perPage * ((page || 1) - 1),
      where: conditions,
      orderBy: {
        id: 'desc',
      },
      select,
    });
    return {
      videos: foundVideosData.map((video) => this.formatVideoResponse(video)),
      pager: { current: page || 1, total: Math.ceil(count / perPage) || 1 },
    };
  }

  public async createVideo(
    file: Express.Multer.File,
    videoData: CreateVideoDto,
    userId: number,
    user: User,
  ): Promise<VideoResponse> {
    if (user.id !== userId) throw new HttpException(401, 'Unauthorized');
    const duration = await this.editor.getVideoDuration(file.path);
    const created = await this.videos.create({
      data: {
        enabled: true,
        name: videoData.name,
        source: path.join('/public/videos', file.filename),
        views: 0,
        duration,
        user: {
          connect: {
            id: userId,
          },
        },
      },
      select,
    });
    return this.formatVideoResponse(created);
  }
}

export default VideoService;
