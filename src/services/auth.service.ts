import { compare } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { PrismaClient, User } from '@prisma/client';
import { SECRET_KEY } from '@config';
import { LoginUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken, TokenData } from '@interfaces/auth.interface';
import { TokenResponse } from '@interfaces/reponses.interface';

class AuthService {
  public users = new PrismaClient().user;

  public async login(userData: LoginUserDto): Promise<{
    cookie: string; token: TokenResponse;
  }> {
    const findUser: User = await this.users.findUnique({ where: { username: userData.login } });
    if (!findUser) throw new HttpException(400, 'Bad Request', 4000, ['Invalid credentials']);

    const isPasswordMatching = await this.comparePassword(
      userData.password,
      findUser.password,
    );
    if (!isPasswordMatching) throw new HttpException(400, 'Bad Request', 4000, ['Invalid credentials']);

    const tokenData = this.createToken(findUser);
    const cookie = this.createCookie(tokenData);

    return {
      cookie,
      token: {
        token: tokenData.token,
        user: {
          created_at: findUser.created_at,
          email: findUser.email,
          id: findUser.id,
          pseudo: findUser.pseudo,
          username: findUser.username,
        },
      },
    };
  }

  // eslint-disable-next-line class-methods-use-this
  public createToken(user: Partial<User>): TokenData {
    const dataStoredInToken: DataStoredInToken = { id: user.id };
    const secretKey: string = SECRET_KEY;
    const expiresIn: number = 60 * 60;

    return { expiresIn, token: sign(dataStoredInToken, secretKey, { expiresIn }) };
  }

  // eslint-disable-next-line class-methods-use-this
  public createCookie(tokenData: TokenData): string {
    return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn};`;
  }

  // eslint-disable-next-line class-methods-use-this
  public async comparePassword(password: string, hash: string): Promise<boolean> {
    return compare(password, hash);
  }
}

export default AuthService;
