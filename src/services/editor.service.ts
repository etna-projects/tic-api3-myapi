import { path as ffmpegpath } from '@ffmpeg-installer/ffmpeg';
import { path as ffprobepath } from '@ffprobe-installer/ffprobe';
import ffmpeg from 'fluent-ffmpeg';
import path from 'path';
import { existsSync } from 'fs';
import { unlink } from 'fs/promises';
import { VideoResolution } from '@/interfaces/video.interface';
import { VideoSize } from '@/utils/enum';

export default class EditorService {
  private ffmpeg = ffmpeg;

  constructor() {
    this.ffmpeg.setFfmpegPath(ffmpegpath);
    this.ffmpeg.setFfprobePath(ffprobepath);
  }

  /**
   *
   * @param filePath - path to video file
   * @returns {Promise<number>} - duration of video in milliseconds rounded to nearest millisecond
   */
  public getVideoDuration(filePath: string) {
    return new Promise<number>((resolve, reject) => {
      this.ffmpeg.ffprobe(filePath, (err, metadata) => {
        if (err) return reject(err);
        return resolve(Math.round(metadata.format.duration * 1000));
      });
    });
  }

  /**
   *
   * @param filePath - path to video file
   * @param resolution - resolution of video
   * @returns {Promise<string>} - path to video file
   */
  public async scaleVideo(filePath: string, resolution: VideoResolution) {
    const fileExtension = path.extname(filePath);
    const fileName = path.basename(filePath, fileExtension);
    const originalPath = path.join(__dirname, '../../', filePath);
    const destPath = path.join(__dirname, '../../public/videos', `${fileName}_${resolution}${fileExtension}`);
    return new Promise<string>((resolve, reject) => {
      if (existsSync(destPath)) {
        resolve(destPath);
      }
      this.ffmpeg(originalPath)
        .keepDisplayAspectRatio()
        .size(VideoSize[resolution])
        .save(destPath)
        .on('error', (err) => reject(err))
        .on('end', () => resolve(destPath))
        .run();
    });
  }

  // eslint-disable-next-line class-methods-use-this
  public async deleteVideo(filePath: string) {
    const originalPath = path.join(__dirname, '../../', filePath);
    await unlink(originalPath);
  }
}
