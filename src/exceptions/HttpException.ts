export class HttpException extends Error {
  public status: number;

  public message: 'Not found' | 'Unauthorized' | 'Bad Request';

  public data?: Array<string>;

  public code?: number;

  constructor(
    status: number,
    message: 'Not found' | 'Unauthorized' | 'Bad Request',
    code?: number,
    data?: Array<string>,
  ) {
    super();
    this.status = status;
    this.message = message;
    this.code = code;
    this.data = data;
  }
}
