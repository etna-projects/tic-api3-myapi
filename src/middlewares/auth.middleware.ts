import { NextFunction, RequestHandler, Response } from 'express';
import { verify } from 'jsonwebtoken';
import { PrismaClient } from '@prisma/client';
import { SECRET_KEY } from '@config';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken, RequestWithUser } from '@interfaces/auth.interface';

const authMiddleware: RequestHandler = async (
  req: RequestWithUser,
  res: Response,
  next: NextFunction,
) => {
  try {
    const Authorization = req.cookies.Authorization || (req.header('Authorization') ? req.header('Authorization').split('Bearer ')[1] : null);

    if (!Authorization) {
      return next(new HttpException(401, 'Unauthorized'));
    }
    const secretKey: string = SECRET_KEY;
    const verificationResponse = (await verify(Authorization, secretKey)) as DataStoredInToken;
    const userId = verificationResponse.id;

    const users = new PrismaClient().user;
    const findUser = await users.findUnique({ where: { id: Number(userId) } });

    if (!findUser) {
      return next(new HttpException(401, 'Unauthorized'));
    }
    req.user = findUser;
    return next();
  } catch (error) {
    return next(new HttpException(401, 'Unauthorized'));
  }
};

export default authMiddleware;
