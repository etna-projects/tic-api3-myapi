import { NextFunction, Request, Response } from 'express';
import { HttpException } from '@exceptions/HttpException';
import { logger } from '@utils/logger';

const errorMiddleware = (error: HttpException, req: Request, res: Response, next: NextFunction) => {
  try {
    const status: number = error.status || 500;
    const message: string = error.message || 'Something went wrong';
    const data: Array<string> = error.data || [];
    const code = error.code || 0;

    // eslint-disable-next-line no-console
    console.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}`);
    logger.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}`);
    if (error.code && error.data) {
      return res.status(status).json({
        code,
        message,
        data,
      });
    }
    return res.status(status).json({ message });
  } catch (err) {
    return next(err);
  }
};

export default errorMiddleware;
