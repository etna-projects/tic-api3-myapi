import { plainToInstance } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import { RequestHandler } from 'express';

const getAllNestedErrors = (error: ValidationError) => {
  if (error.constraints) {
    return Object.values(error.constraints);
  }
  return error.children.map(getAllNestedErrors).join(',');
};

export const validationMiddleware = (
  type: any,
  value: string | 'body' | 'query' | 'params' = 'body',
  skipMissingProperties = false,
  whitelist = true,
  forbidNonWhitelisted = true,
): RequestHandler => (req, res, next) => {
  const obj = plainToInstance(type, req[value]);
  validate(
    obj,
    { skipMissingProperties, whitelist, forbidNonWhitelisted },
  ).then((errors: ValidationError[]) => {
    if (errors.length > 0) {
      const stack = errors.map(getAllNestedErrors);
      return res.status(400).json({
        message: 'Bad Request',
        code: 1001,
        data: stack,
      });
    }
    return next();
  });
};
