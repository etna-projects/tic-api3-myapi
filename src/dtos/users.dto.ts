/* eslint-disable max-classes-per-file */
import {
  IsEmail, IsOptional, IsString, Matches,
} from 'class-validator';
import { PagerDto } from './pager.dto';

export class CreateUserDto {
  @IsEmail()
  public email: string;

  @IsString()
  public password: string;

  @IsString()
  @Matches(/[a-zA-Z0-9_-]/)
  public username: string;

  @IsString()
  public pseudo: string;
}

export class UpdateUserDto {
  @IsOptional()
  @IsString()
  public email?: string;

  @IsOptional()
  @IsString()
  public password?: string;

  @IsOptional()
  @IsString()
  @Matches(/[a-zA-Z0-9_-]/)
  public username?: string;

  @IsOptional()
  @IsString()
  public pseudo?: string;
}

export class LoginUserDto {
  @IsString()
  @Matches(/[a-zA-Z0-9_-]/)
  public login: string;

  @IsString()
  public password: string;
}

export class ListUsersQuery extends PagerDto {
  @IsString()
  @IsOptional()
  public pseudo?: string;
}
