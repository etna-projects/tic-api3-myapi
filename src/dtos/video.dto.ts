/* eslint-disable max-classes-per-file */
import {
  IsEnum,
  IsIn,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Validate,
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { VideoResolution } from '@/interfaces/video.interface';
import { PagerDto } from './pager.dto';
import { VideoSize } from '@/utils/enum';

@ValidatorConstraint({ name: 'string-or-number', async: false })
export class IsNumberOrString implements ValidatorConstraintInterface {
  // eslint-disable-next-line class-methods-use-this
  validate(text: any) {
    return typeof text === 'number' || typeof text === 'string';
  }

  // eslint-disable-next-line class-methods-use-this
  defaultMessage({ property }: ValidationArguments) {
    return `(${property}) must be number or string`;
  }
}

export class CreateVideoDto {
  @IsNotEmpty()
  @IsString()
  public name: string;
}

export class FormatVideoDto {
  @IsNotEmpty()
  @IsIn(['144', '240', '360', '480', '720', '1080'])
  public format: '144' | '240' | '360' | '480' | '720' | '1080';
}

export class ListVideosQuery extends PagerDto {
  @IsOptional()
  @IsString()
  public user?: string;

  @IsOptional()
  @IsString()
    name?: string;

  @IsOptional()
  @IsInt()
    duration?: number;
}

export class UpdateVideoDto {
  @IsOptional()
  @IsString()
  public name?: string;

  @IsOptional()
  @IsInt()
  public user: number;
}

export class ScaleVideoDto {
  @IsString()
  public file: string;

  @IsIn(Object.keys(VideoSize))
  public format: VideoResolution;
}
