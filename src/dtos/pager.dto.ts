import 'reflect-metadata';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';

export class PagerDto {
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  public page?: number;

  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  public perPage?: number;
}
