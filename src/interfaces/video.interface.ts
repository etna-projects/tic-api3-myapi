import { Video_format } from '@prisma/client';

export type VideoResolution = '144' | '240' | '360' | '480' | '720' | '1080';

export type VideoResolutionMap = { [resolution in VideoResolution]: string };

export interface GenericVideoOutput {
  id: number;
  created_at: Date;
  name: string;
  duration: number;
  source: string;
  views: number;
  enabled: boolean;
  user: {
    id: number;
    username: string;
    email: string;
    pseudo: string;
    created_at: Date;
  };
  video_format: Video_format[];
}
