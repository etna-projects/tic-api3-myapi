import { Request } from 'express';
import { User } from '@prisma/client';

export interface DataStoredInToken {
  id: number;
}

export interface TokenData {
  token: string;
  expiresIn: number;
}

export type RequestWithUser<T = {}> = Request<T> & {
  user: User;
};
