export interface UserResponse {
  id: number;
  username: string;
  pseudo: string;
  created_at: Date;
  email: string;
}

export interface VideoFormatsResponse {
  '1080': string;
  '720': string;
  '480': string;
  '360': string;
  '240': string;
  '144': string;
}

export interface VideoResponse {
  id: number;
  source: string;
  name?: string;
  created_at: Date;
  views: number;
  enabled: boolean;
  user: UserResponse;
  format: VideoFormatsResponse;
}

export interface TokenResponse {
  token: string;
  user: UserResponse;
}

export interface CommentResponse {
  id: number;
  body: string;
  user: UserResponse;
}
