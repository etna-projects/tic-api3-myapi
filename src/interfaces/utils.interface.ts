export interface PagerInfo {
  current: number;
  total: number;
}
