import { faker } from '@faker-js/faker';
import { PrismaClient, User } from '@prisma/client';
import dotenv from 'dotenv';
import RandExp from 'randexp';
import request from 'supertest';
import path from 'path';
import UserService from '@/services/users.service';
import AuthService from '@/services/auth.service';
import { CreateUserDto } from '@/dtos/users.dto';
import { VideoController } from '@/controllers/video.controller';
import App from '@/app';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;
const userService = new UserService();
const authService = new AuthService();

const userData: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.name.firstName(),
  username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
};

let token: string;

beforeAll(async () => {
  await users.deleteMany({});
  const user = await userService.createUser(userData as CreateUserDto);
  const tokenData = await authService.createToken(user);
  token = tokenData.token;
  userData.id = user.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('09-video-list-by-user', () => {
  describe('GET /user/:id/videos', () => {
    it('List all the user\'s videos without authorization requirement', async () => {
      const app = new App([VideoController]);
      const videoName = faker.random.word();
      const server = app.getServer();
      expect(userData.id).toBeDefined();
      await request(server)
        .post(`/user/${userData.id}/video`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .field('name', videoName)
        .attach('source', path.join(__dirname, 'assets/happy_money_guy.mov'));

      const response = await request(server)
        .get(`/user/${userData.id}/videos`)
        .accept('application/json');
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveLength(1);
      const receivedVideo = response.body.data[0];
      expect(receivedVideo).toHaveProperty('id');
      expect(receivedVideo).toHaveProperty('source');
      expect(receivedVideo).toHaveProperty('format');
      expect(receivedVideo).toHaveProperty('user');
      expect(receivedVideo).toHaveProperty('created_at');
      expect(receivedVideo.format).toBeDefined();
      expect(receivedVideo.format).toHaveProperty('1080');
      expect(receivedVideo.format).toHaveProperty('720');
      expect(receivedVideo.format).toHaveProperty('480');
      expect(receivedVideo.format).toHaveProperty('360');
      expect(receivedVideo.format).toHaveProperty('240');
      expect(receivedVideo.format).toHaveProperty('144');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
    });
  });
});
