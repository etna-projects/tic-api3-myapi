import { faker } from '@faker-js/faker';
import {
  Comment,
  PrismaClient, User, Video,
} from '@prisma/client';
import dotenv from 'dotenv';
import request from 'supertest';
import AuthService from '@/services/auth.service';
import App from '@/app';
import { CommentController } from '@/controllers/comment.controller';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;
const videos = prisma.video;
const comments = prisma.comment;
const authService = new AuthService();

const authorUser: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.internet.userName(),
  username: faker.internet.userName(),
};
const commenterUser: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.internet.userName(),
  username: faker.internet.userName(),
};
const video: Partial<Video> = {
  duration: 3000,
  enabled: true,
  name: faker.word.noun(),
  source: faker.image.imageUrl(),
  views: 0,
};

let token: string;

beforeEach(async () => {
  await users.deleteMany({});
  const createdAuthor = await users.create({ data: { ...authorUser, id: undefined } as User });
  const commenter = await users.create({ data: { ...commenterUser, id: undefined } as User });
  const createdVideo = await videos.create({
    data: {
      user: { connect: { id: createdAuthor.id } },
      ...video,
      id: undefined,
    } as Video,
  });
  await comments.createMany({
    data: Array.from({ length: 10 }, () => ({
      user_id: commenter.id,
      video_id: createdVideo.id,
      body: faker.lorem.sentence(),
    })),
  });
  const tokenData = await authService.createToken(commenter);
  token = tokenData.token;
  authorUser.id = createdAuthor.id;
  video.id = createdVideo.id;
  commenterUser.id = commenter.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('14-comment-list', () => {
  describe('GET /video/:id/comments', () => {
    it('creates a comment on a video', async () => {
      const app = new App([CommentController]);
      const server = app.getServer();
      expect(video.id).toBeDefined();
      expect(authorUser.id).toBeDefined();
      expect(commenterUser.id).toBeDefined();
      const response = await request(server)
        .get(`/video/${video.id}/comments`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`);
      expect(response.statusCode).toBe(200);
      expect(response.body.data).toHaveLength(5);
    }, 30000);
  });
});
