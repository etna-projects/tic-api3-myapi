import { faker } from '@faker-js/faker';
import {
  PrismaClient, User, Video,
} from '@prisma/client';
import dotenv from 'dotenv';
import request from 'supertest';
import AuthService from '@/services/auth.service';
import App from '@/app';
import { CommentController } from '@/controllers/comment.controller';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;
const videos = prisma.video;
const comments = prisma.comment;
const authService = new AuthService();

const authorUser: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.internet.userName(),
  username: faker.internet.userName(),
};
const commenterUser: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.internet.userName(),
  username: faker.internet.userName(),
};
const video: Partial<Video> = {
  duration: 3000,
  enabled: true,
  name: faker.word.noun(),
  source: faker.image.imageUrl(),
  views: 0,
};

let token: string;

beforeEach(async () => {
  await users.deleteMany({});
  const createdAuthor = await users.create({ data: { ...authorUser, id: undefined } as User });
  const commenter = await users.create({ data: { ...commenterUser, id: undefined } as User });
  const createdVideo = await videos.create({
    data: {
      user: { connect: { id: createdAuthor.id } },
      ...video,
      id: undefined,
    } as Video,
  });
  const tokenData = await authService.createToken(commenter);
  token = tokenData.token;
  authorUser.id = createdAuthor.id;
  video.id = createdVideo.id;
  commenterUser.id = commenter.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('13-comment-creation', () => {
  describe('POST /video/:id/comment', () => {
    it('creates a comment on a video', async () => {
      const app = new App([CommentController]);
      const server = app.getServer();
      const comment = faker.lorem.sentence();
      expect(video.id).toBeDefined();
      expect(authorUser.id).toBeDefined();
      expect(commenterUser.id).toBeDefined();
      const response = await request(server)
        .post(`/video/${video.id}/comment`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .send({ body: comment });
      expect(response.statusCode).toBe(201);
      expect(response.body.data.body).toBe(comment);
      const videoComment = await comments.findFirst({
        where: { video: { id: video.id, user: { id: commenterUser.id } } },
      });
      expect(videoComment).toBeDefined();
    }, 30000);
  });
});
