import { faker } from '@faker-js/faker';
import { PrismaClient, User } from '@prisma/client';
import dotenv from 'dotenv';
import RandExp from 'randexp';
import request from 'supertest';
import path from 'path';
import UserService from '@/services/users.service';
import AuthService from '@/services/auth.service';
import { CreateUserDto } from '@/dtos/users.dto';
import { VideoController } from '@/controllers/video.controller';
import App from '@/app';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;
const userService = new UserService();
const authService = new AuthService();

const userData: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.name.firstName(),
  username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
};

let token: string;

beforeAll(async () => {
  await users.deleteMany({});
  const user = await userService.createUser(userData as CreateUserDto);
  const tokenData = await authService.createToken(user);
  token = tokenData.token;
  userData.id = user.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('07-video-creation', () => {
  describe('POST /user/:id/video', () => {
    it('Should not create the video without authentication', async () => {
      const app = new App([VideoController]);
      const response = await request(app.getServer())
        .post(`/user/${userData.id}/video`)
        .accept('application/json');
      expect(response.statusCode).toBe(401);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('unauthorized');
    });

    it('creates a video in database and save the file in public/video folder', async () => {
      const app = new App([VideoController]);
      const videoName = faker.random.word();
      const response = await request(app.getServer())
        .post(`/user/${userData.id}/video`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .field('name', videoName)
        .attach('source', path.join(__dirname, 'assets/happy_money_guy.mov'));
      expect(response.statusCode).toBe(201);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveProperty('id');
      expect(response.body.data).toHaveProperty('source');
      expect(response.body.data).toHaveProperty('format');
      expect(response.body.data).toHaveProperty('user');
      expect(response.body.data).toHaveProperty('created_at');
      expect(response.body.data.id).toBeDefined();
      expect(response.body.data.source).toBeDefined();
      expect(response.body.data.source).toInclude('happymoneyguy.mov');
      expect(response.body.data.source).toInclude('/public/videos/');
      expect(response.body.data.format).toBeDefined();
      expect(response.body.data.format).toHaveProperty('1080');
      expect(response.body.data.format).toHaveProperty('720');
      expect(response.body.data.format).toHaveProperty('480');
      expect(response.body.data.format).toHaveProperty('360');
      expect(response.body.data.format).toHaveProperty('240');
      expect(response.body.data.format).toHaveProperty('144');
      expect(response.body.data.user).toHaveProperty('id');
      expect(response.body.data.user).toHaveProperty('email');
      expect(response.body.data.user).not.toHaveProperty('password');
      expect(response.body.data.user).toHaveProperty('pseudo');
      expect(response.body.data.user).toHaveProperty('username');
      expect(response.body.data.user).toHaveProperty('created_at');
      const videoResponse = await request(app.getServer())
        .get(response.body.data.source);
      expect(videoResponse.statusCode).toBe(200);
      expect(videoResponse.body).toBeDefined();
      // check if videoResponse is a video vie response headers
      expect(videoResponse.headers).toHaveProperty('content-type');
      expect(videoResponse.headers['content-type']).toBe('video/quicktime');
      expect(videoResponse.headers).toHaveProperty('content-length');
      expect(videoResponse.headers['content-length']).toBeDefined();
      expect(videoResponse.headers['content-length']).not.toBe(0);
    }, 10000);
  });
});
