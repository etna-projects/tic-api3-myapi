import { faker } from '@faker-js/faker';
import { PrismaClient, User } from '@prisma/client';
import dotenv from 'dotenv';
import RandExp from 'randexp';
import request from 'supertest';
import { UserController } from '@/controllers/user.controller';
import App from '@/app';
import { CreateUserDto } from '@/dtos/users.dto';
import UserService from '@/services/users.service';
import AuthService from '@/services/auth.service';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;
const userService = new UserService();
const authService = new AuthService();

const userData: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.name.firstName(),
  username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
};

let token: string;

beforeAll(async () => {
  await users.deleteMany({});
  const user = await userService.createUser(userData as CreateUserDto);
  const tokenData = await authService.createToken(user);
  token = tokenData.token;
  userData.id = user.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('06-user-by-id', () => {
  describe('GET /user/:id', () => {
    it('Should not get user without authentication', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .get(`/user/${userData.id}`)
        .accept('application/json');
      expect(response.statusCode).toBe(401);
      expect(response.body.message).toBe('Unauthorized');
    });

    it('Gets user by id', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .get(`/user/${userData.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`);
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveProperty('id');
      expect(response.body.data).toHaveProperty('email');
      expect(response.body.data).not.toHaveProperty('password');
      expect(response.body.data).toHaveProperty('pseudo');
      expect(response.body.data).toHaveProperty('username');
      expect(response.body.data).toHaveProperty('created_at');
      expect(response.body.data.created_at).toBeDefined();
      expect(response.body.data.id).toBe(userData.id);
      expect(response.body.data.email).toBe(userData.email);
      expect(response.body.data.pseudo).toBe(userData.pseudo);
      expect(response.body.data.username).toBe(userData.username);
    });
  });
});
