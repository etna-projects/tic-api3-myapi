import { faker } from '@faker-js/faker';
import RandExp from 'randexp';
import { PrismaClient, User } from '@prisma/client';
import dotenv from 'dotenv';
import request from 'supertest';
import App from '@/app';
import { UserController } from '@/controllers/user.controller';
import { CreateUserDto } from '@/dtos/users.dto';
import AuthService from '@/services/auth.service';
import UserService from '@/services/users.service';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;

const userService = new UserService();
const authService = new AuthService();

let user: Partial<User>;

let updatedUser: Partial<User>;

let token: string;

beforeEach(async () => {
  await users.deleteMany({});
  user = {
    email: faker.internet.email(),
    password: faker.internet.password(),
    pseudo: faker.name.firstName(),
    username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
  };
  updatedUser = {
    email: faker.internet.email(),
    password: faker.internet.password(),
    pseudo: faker.name.firstName(),
    username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
  };
  const createdUser = await userService.createUser(user as CreateUserDto);
  const tokenData = await authService.createToken(createdUser);
  token = tokenData.token;
  user.id = createdUser.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('04-user-update', () => {
  describe('PUT /user/:id', () => {
    it('Should not update without autorization', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .put(`/user/${user.id}`)
        .accept('application/json')
        .send({ username: updatedUser.username });
      expect(response.statusCode).toBe(401);
      expect(response.body.message).toBe('Unauthorized');
    });

    it('Should not update another user', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .put(`/user/${user.id + 1}`)
        .accept('application/json')
        .send({ username: updatedUser.username });
      expect(response.statusCode).toBe(401);
      expect(response.body.message).toBe('Unauthorized');
    });

    it('Updates all user\'s properties', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .put(`/user/${user.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .send(updatedUser);
      const foundUser = await users.findFirst({ where: { id: user.id } });
      const isPasswordUpdated = await authService.comparePassword(
        updatedUser.password,
        foundUser.password,
      );
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveProperty('id');
      expect(response.body.data).toHaveProperty('email');
      expect(response.body.data).not.toHaveProperty('password');
      expect(response.body.data).toHaveProperty('pseudo');
      expect(response.body.data).toHaveProperty('username');
      expect(response.body.data).toHaveProperty('created_at');
      expect(response.body.data.created_at).toBeDefined();
      expect(foundUser).not.toBe(null);
      expect(foundUser.email).toBe(updatedUser.email);
      expect(foundUser.pseudo).toBe(updatedUser.pseudo);
      expect(foundUser.username).toBe(updatedUser.username);
      expect(isPasswordUpdated).toBe(true);
    });

    it('Updates only one user property', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .put(`/user/${user.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .send({ username: updatedUser.username });
      const foundUser = await users.findFirst({ where: { id: user.id } });
      const isSamePassword = await authService.comparePassword(
        user.password,
        foundUser.password,
      );
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveProperty('id');
      expect(response.body.data).toHaveProperty('email');
      expect(response.body.data).not.toHaveProperty('password');
      expect(response.body.data).toHaveProperty('pseudo');
      expect(response.body.data).toHaveProperty('username');
      expect(response.body.data).toHaveProperty('created_at');
      expect(response.body.data.created_at).toBeDefined();
      expect(foundUser).not.toBe(null);
      expect(foundUser.email).toBe(user.email);
      expect(foundUser.pseudo).toBe(user.pseudo);
      expect(foundUser.username).toBe(updatedUser.username);
      expect(isSamePassword).toBe(true);
    });
  });
});
