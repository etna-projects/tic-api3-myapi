import dotenv from 'dotenv';
import request from 'supertest';
import { faker } from '@faker-js/faker';
import { UserController } from '@controllers/user.controller';
import { PrismaClient } from '@prisma/client';
import RandExp from 'randexp';
import App from '@/app';
import { CreateUserDto } from '@/dtos/users.dto';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;

beforeAll(async () => {
  await users.deleteMany({});
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('01-user-creation', () => {
  describe('POST /user', () => {
    it('Create user and send back the user object in response body', async () => {
      const app = new App([UserController]);
      const userData: CreateUserDto = {
        email: faker.internet.email(),
        password: faker.internet.password(),
        pseudo: faker.name.firstName(),
        username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
      };
      const response = await request(app.getServer())
        .post('/user')
        .accept('application/json')
        .send(userData);
      expect(response.statusCode).toBe(201);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveProperty('id');
      expect(response.body.data).toHaveProperty('email');
      expect(response.body.data).not.toHaveProperty('password');
      expect(response.body.data).toHaveProperty('pseudo');
      expect(response.body.data).toHaveProperty('username');
      expect(response.body.data).toHaveProperty('created_at');
      expect(response.body.data.created_at).toBeDefined();
    });

    it('Fail creating the user when there is a missing field', async () => {
      const app = new App([UserController]);
      const userData = {
        email: faker.internet.email(),
        password: faker.internet.password(),
      };
      const response = await request(app.getServer())
        .post('/user')
        .accept('application/json')
        .send(userData);
      expect(response.statusCode).toBe(400);
    });
  });
});
