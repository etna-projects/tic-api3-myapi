import { faker } from '@faker-js/faker';
import { PrismaClient, User } from '@prisma/client';
import dotenv from 'dotenv';
import request from 'supertest';
import App from '@/app';
import { UserController } from '@/controllers/user.controller';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;

const userList: Partial<User>[] = Array.from({ length: 20 }).map(() => ({
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.name.firstName(),
  username: faker.internet.userName(),
}));

beforeAll(async () => {
  await users.deleteMany({});
  await users.createMany({
    data: userList as User[],
  });
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('05-user-list', () => {
  describe('GET /users', () => {
    it('Gets users', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .get('/users?page=1&perPage=30')
        .accept('application/json');
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveLength(userList.length);
      // expect each element of response.body.data to have the same properties as userList
      response.body.data.forEach((user) => {
        expect(user).toHaveProperty('id');
        expect(user).toHaveProperty('email');
        expect(user).not.toHaveProperty('password');
        expect(user).toHaveProperty('pseudo');
        expect(user).toHaveProperty('username');
      });
    });

    it('Gets users with pagination', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .get('/users?page=2&perPage=5')
        .accept('application/json');
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveLength(5);
      // expect each element of response.body.data to have the same properties as userList
      response.body.data.forEach((user) => {
        expect(user).toHaveProperty('id');
        expect(user).toHaveProperty('email');
        expect(user).not.toHaveProperty('password');
        expect(user).toHaveProperty('pseudo');
        expect(user).toHaveProperty('username');
      });
    });

    it('Gets users without pagination', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .get('/users')
        .accept('application/json');
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toHaveLength(5);
      // expect each element of response.body.data to have the same properties as userList
      response.body.data.forEach((user) => {
        expect(user).toHaveProperty('id');
        expect(user).toHaveProperty('email');
        expect(user).not.toHaveProperty('password');
        expect(user).toHaveProperty('pseudo');
        expect(user).toHaveProperty('username');
      });
    });
  });
});
