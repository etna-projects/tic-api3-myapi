import dotenv from 'dotenv';
import request from 'supertest';
import { faker } from '@faker-js/faker';
import { PrismaClient, User } from '@prisma/client';
import RandExp from 'randexp';
import App from '@/app';
import { CreateUserDto } from '@/dtos/users.dto';
import UserService from '@/services/users.service';
import AuthService from '@/services/auth.service';
import { UserController } from '@/controllers/user.controller';

dotenv.config({ path: '../.env.test.local' });
const prisma = new PrismaClient();
const users = prisma.user;

const userService = new UserService();
const authService = new AuthService();

const user: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.name.firstName(),
  username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
};

let token: string;

beforeAll(async () => {
  await users.deleteMany({});
  const createdUser = await userService.createUser(user as CreateUserDto);
  const tokenData = await authService.createToken(createdUser);
  token = tokenData.token;
  user.id = createdUser.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('03-user-deletion', () => {
  describe('DELETE /user/:id', () => {
    it('Should not delete another user', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .delete(`/user/${user.id + 1}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`);
      expect(response.statusCode).toBe(401);
      expect(response.body.message).toBe('Unauthorized');
    });

    it('Should not delete without authentication', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .delete(`/user/${user.id}`)
        .accept('application/json');
      expect(response.statusCode).toBe(401);
      expect(response.body.message).toBe('Unauthorized');
    });

    it('Deletes the user', async () => {
      const app = new App([UserController]);
      const response = await request(app.getServer())
        .delete(`/user/${user.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`);
      const foundUser = await users.findFirst({ where: { id: user.id } });
      expect(response.statusCode).toBe(204);
      expect(response.body).not.toBe(null);
      expect(foundUser).toBe(null);
    });
  });
});
