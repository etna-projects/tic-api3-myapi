import { faker } from '@faker-js/faker';
import {
  PrismaClient, User, Video,
} from '@prisma/client';
import dotenv from 'dotenv';
import request from 'supertest';
import AuthService from '@/services/auth.service';
import { VideoController } from '@/controllers/video.controller';
import App from '@/app';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;
const videos = prisma.video;
const authService = new AuthService();

const authorUser: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.internet.userName(),
  username: faker.internet.userName(),
};
const receiverUser: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.internet.userName(),
  username: faker.internet.userName(),
};
const video: Partial<Video> = {
  duration: 3000,
  enabled: true,
  name: faker.word.noun(),
  source: faker.image.imageUrl(),
  views: 0,
};

let token: string;

beforeEach(async () => {
  await users.deleteMany({});
  const createdAuthor = await users.create({ data: authorUser as User });
  const createdReceiver = await users.create({ data: receiverUser as User });
  const createdVideo = await videos.create({
    data: {
      user: { connect: { id: createdAuthor.id } },
      ...video,
      id: undefined,
    } as Video,
  });
  const tokenData = await authService.createToken(createdAuthor);
  token = tokenData.token;
  authorUser.id = createdAuthor.id;
  receiverUser.id = createdReceiver.id;
  video.id = createdVideo.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('11-video-update', () => {
  describe('PUT /video/:id', () => {
    it('updates the video\'s name and author', async () => {
      const app = new App([VideoController]);
      const videoName = faker.random.word();
      const server = app.getServer();
      expect(video.id).toBeDefined();
      expect(authorUser.id).toBeDefined();
      expect(receiverUser.id).toBeDefined();
      const response = await request(server)
        .put(`/video/${video.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .send({ name: videoName, user: receiverUser.id });
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      const receivedVideo = response.body.data;
      expect(receivedVideo).toHaveProperty('id');
      expect(receivedVideo).toHaveProperty('source');
      expect(receivedVideo).toHaveProperty('format');
      expect(receivedVideo).toHaveProperty('user');
      expect(receivedVideo).toHaveProperty('created_at');
      expect(receivedVideo.format).toBeDefined();
      expect(receivedVideo.format).toHaveProperty('1080');
      expect(receivedVideo.format).toHaveProperty('720');
      expect(receivedVideo.format).toHaveProperty('480');
      expect(receivedVideo.format).toHaveProperty('360');
      expect(receivedVideo.format).toHaveProperty('240');
      expect(receivedVideo.format).toHaveProperty('144');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
      const foundVideo = await videos.findFirst({
        where: { id: video.id },
        include: { user: true },
      });
      expect(foundVideo.user.id).toBe(receiverUser.id);
      expect(foundVideo.name).toBe(videoName);
    }, 30000);

    it('Updates only the video name', async () => {
      const app = new App([VideoController]);
      const videoName = faker.random.word();
      const server = app.getServer();
      expect(video.id).toBeDefined();
      expect(authorUser.id).toBeDefined();
      expect(receiverUser.id).toBeDefined();
      const response = await request(server)
        .put(`/video/${video.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .send({ name: videoName });
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      const receivedVideo = response.body.data;
      expect(receivedVideo).toHaveProperty('id');
      expect(receivedVideo).toHaveProperty('source');
      expect(receivedVideo).toHaveProperty('format');
      expect(receivedVideo).toHaveProperty('user');
      expect(receivedVideo).toHaveProperty('created_at');
      expect(receivedVideo.format).toBeDefined();
      expect(receivedVideo.format).toHaveProperty('1080');
      expect(receivedVideo.format).toHaveProperty('720');
      expect(receivedVideo.format).toHaveProperty('480');
      expect(receivedVideo.format).toHaveProperty('360');
      expect(receivedVideo.format).toHaveProperty('240');
      expect(receivedVideo.format).toHaveProperty('144');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
      const foundVideo = await videos.findFirst({
        where: { id: video.id },
        include: { user: true },
      });
      expect(foundVideo.user.id).toBe(authorUser.id);
      expect(foundVideo.name).toBe(videoName);
    }, 30000);

    it('Updates only the video author', async () => {
      const app = new App([VideoController]);
      const server = app.getServer();
      expect(video.id).toBeDefined();
      expect(authorUser.id).toBeDefined();
      expect(receiverUser.id).toBeDefined();
      const response = await request(server)
        .put(`/video/${video.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .send({ user: receiverUser.id });
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      const receivedVideo = response.body.data;
      expect(receivedVideo).toHaveProperty('id');
      expect(receivedVideo).toHaveProperty('source');
      expect(receivedVideo).toHaveProperty('format');
      expect(receivedVideo).toHaveProperty('user');
      expect(receivedVideo).toHaveProperty('created_at');
      expect(receivedVideo.format).toBeDefined();
      expect(receivedVideo.format).toHaveProperty('1080');
      expect(receivedVideo.format).toHaveProperty('720');
      expect(receivedVideo.format).toHaveProperty('480');
      expect(receivedVideo.format).toHaveProperty('360');
      expect(receivedVideo.format).toHaveProperty('240');
      expect(receivedVideo.format).toHaveProperty('144');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
      expect(receivedVideo.user).toHaveProperty('id');
      expect(receivedVideo.user).toHaveProperty('email');
      expect(receivedVideo.user).not.toHaveProperty('password');
      expect(receivedVideo.user).toHaveProperty('pseudo');
      expect(receivedVideo.user).toHaveProperty('username');
      expect(receivedVideo.user).toHaveProperty('created_at');
      const foundVideo = await videos.findFirst({
        where: { id: video.id },
        include: { user: true },
      });
      expect(foundVideo.user.id).toBe(receiverUser.id);
      expect(foundVideo.name).toBe(video.name);
    }, 30000);
  });
});
