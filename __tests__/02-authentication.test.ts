import dotenv from 'dotenv';
import request from 'supertest';
import { faker } from '@faker-js/faker';
import { AuthController } from '@controllers/auth.controller';
import { PrismaClient } from '@prisma/client';
import RandExp from 'randexp';
import App from '@/app';
import { CreateUserDto } from '@/dtos/users.dto';
import UserService from '@/services/users.service';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;

const userService = new UserService();

const userData: CreateUserDto = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.name.firstName(),
  username: new RandExp(/[a-zA-Z0-9_-]/).gen(),
};

beforeAll(async () => {
  await users.deleteMany({});
  await userService.createUser(userData);
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('02-authentication', () => {
  describe('POST /auth', () => {
    it('Logs user in, sets authorization cookie and sends back the token in body', async () => {
      const app = new App([AuthController]);
      const response = await request(app.getServer())
        .post('/auth')
        .accept('application/json')
        .send({ login: userData.username, password: userData.password })
        .expect('Set-Cookie', /^Authorization=.+/);
      expect(response.statusCode).toBe(201);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('ok');
      expect(response.body).toHaveProperty('data');
      expect(response.body.data).toBeDefined();
      expect(response.body.data).toHaveProperty('token');
      expect(response.body.data.token).toBeDefined();
      expect(response.body.data).toHaveProperty('user');
      expect(response.body.data.user).toBeDefined();
      expect(response.body.data.user).toHaveProperty('id');
      expect(response.body.data.user.id).toBeDefined();
      expect(response.body.data.user).toHaveProperty('username');
      expect(response.body.data.user.username).toBeDefined();
      expect(response.body.data.user.username).toBe(userData.username);
      expect(response.body.data.user).toHaveProperty('pseudo');
      expect(response.body.data.user.pseudo).toBeDefined();
      expect(response.body.data.user.pseudo).toBe(userData.pseudo);
      expect(response.body.data.user).toHaveProperty('email');
      expect(response.body.data.user.email).toBeDefined();
      expect(response.body.data.user.email).toBe(userData.email);
      expect(response.body.data.user).toHaveProperty('created_at');
      expect(response.body.data.user.created_at).toBeDefined();
      expect(response.body.data.user).not.toHaveProperty('password');
    });

    it('Fail loging in the user because of wrong credentials', async () => {
      const app = new App([AuthController]);
      const response = await request(app.getServer())
        .post('/auth')
        .accept('application/json')
        .send({ login: userData.username, password: 'test' });
      expect(response.statusCode).toBe(400);
      expect(response.body).toHaveProperty('message');
      expect(response.body.message.toLowerCase()).toBe('bad request');
    });
  });
});
