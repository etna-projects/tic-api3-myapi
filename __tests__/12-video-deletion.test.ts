import { faker } from '@faker-js/faker';
import {
  PrismaClient, User, Video,
} from '@prisma/client';
import dotenv from 'dotenv';
import request from 'supertest';
import path from 'path';
import AuthService from '@/services/auth.service';
import { VideoController } from '@/controllers/video.controller';
import App from '@/app';

dotenv.config({ path: '../.env.test.local' });

const prisma = new PrismaClient();
const users = prisma.user;
const videos = prisma.video;
const authService = new AuthService();

const authorUser: Partial<User> = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  pseudo: faker.internet.userName(),
  username: faker.internet.userName(),
};

let token: string;

beforeEach(async () => {
  await users.deleteMany({});
  const createdAuthor = await users.create({ data: authorUser as User });
  const tokenData = await authService.createToken(createdAuthor);
  token = tokenData.token;
  authorUser.id = createdAuthor.id;
});

afterAll(async () => {
  await prisma.$disconnect();
});

describe('12-video-deletion', () => {
  describe('DELETE /video/:id', () => {
    it('Deletes the video', async () => {
      const app = new App([VideoController]);
      const server = app.getServer();

      expect(authorUser.id).toBeDefined();

      const videoName = faker.random.word();

      const { body: { data: video } } = await request(server)
        .post(`/user/${authorUser.id}/video`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`)
        .field('name', videoName)
        .attach('source', path.join(__dirname, 'assets/happy_money_guy.mov'));

      const response = await request(server)
        .delete(`/video/${video.id}`)
        .accept('application/json')
        .set('Authorization', `Bearer ${token}`);
      expect(response.statusCode).toBe(204);
      const foundVideo = await videos.findFirst({
        where: { id: video.id },
        include: { user: true },
      });
      expect(foundVideo).toBeNull();
    }, 30000);
  });
});
